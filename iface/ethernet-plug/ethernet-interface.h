#pragma once

#include <QObject>
#include <plugins/abstract-interface.h>

namespace pairing {

class EthernetInterface : public QObject, public AbstractInterface {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "pairing.InterfacePlugin")
    Q_INTERFACES(AbstractInterface)

public:
    explicit EthernetInterface(QObject *parent = nullptr);

    virtual QString config() const override;

    virtual bool open(const QString & configString) override;
    virtual bool close() override;

    virtual int read(char * data, int maxSize, int timeout, const QString & address) override;
    virtual QByteArray read(int maxSize, int timeout, const QString & address, bool * ok) override;

    virtual int write(const char * data, int size, int timeout, const QString & address) override;

    virtual AbstractInterface * createAnother() const override;

private:
    AbstractInterface * m_impl = nullptr;
};

}
