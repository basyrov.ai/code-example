#pragma once

#include <QtGlobal>

#include <inttypes.h>
#include <unistd.h>

class QByteArray;
class QString;

struct in_addr;
struct sockaddr;
struct sockaddr_in;

namespace pairing { namespace sys {

int socket(int domain, int type, int protocol);
int getsockopt(int descriptor, int level, int optname);
bool setsockopt(int descriptor, int level, int optname, const void * optval, socklen_t optlen);
bool setsockopt(int descriptor, int level, int optname, int optval);
bool inet_aton(const char * addrStr, struct in_addr * addr);

bool bind(int descriptor, const struct sockaddr * addr, socklen_t len);

int open(const QString & devpath, int flags);
bool close(int descriptor);

struct timeval timeval(int usec);

bool rselect(int descriptor, int usec, bool * ok = nullptr);
bool wselect(int descriptor, int usec, bool * ok = nullptr);

int pendingSize(int descriptor);

ssize_t read(int descriptor, char * data, size_t size);
int read(int descriptor, char *data, int maxSize, int timeout);
QByteArray read(int descriptor, int maxSize, int timeout, bool * ok = nullptr);

ssize_t recv(int socket, char * data, size_t size, int flags);
int recv(int socket, char *data, int maxSize, int flags, int timeout);
QByteArray recv(int socket, int maxSize, int flags, int timeout, bool * ok = nullptr);

ssize_t write(int descriptor, const char * data, size_t size);
int write(int descriptor, const char * data, int size, int timeout);
ssize_t send(int descriptor, const char * data, size_t size);
ssize_t send(int descriptor, const char * data, size_t size, int timeout);
ssize_t sendto(int descriptor, const char * data, size_t size, struct sockaddr * addr, socklen_t addrSize);
ssize_t sendto(int descriptor, const char * data, size_t size, struct sockaddr_in * addr, int timeout);

bool connect(int descriptor, const struct sockaddr * addr, socklen_t addrlen);
bool listen(int descriptor, int backlog);
int accept(int descriptor, struct sockaddr * addr, socklen_t * addrlen);

}}
