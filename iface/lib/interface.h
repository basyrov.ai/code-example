#pragma once

#include <QScopedPointer>
#include <QString>

namespace pairing {

class InterfacePrivate;
class Interface {
public:
    ~Interface();
    Interface();
    Interface(const QString & configFilePath);

public:
    bool open(const QString & configFilePath);
    bool close();

    Interface & noserial();
    Interface & serial();
    Interface & serial(int bufferSize);

    Interface & nodebug();
    Interface & debug();
    Interface & debug(int word, int base);

    bool isOpened() const;
    bool isSerial() const;
    bool isDebug() const;

    QByteArray readAll(int timeout = -1, const QString & address = QString(), bool * ok = nullptr);
    QByteArray readAll(int * timeout, const QString & address = QString(), bool * ok = nullptr);

    QByteArray read(int maxSize, int timeout = -1, const QString & address = QString(), bool * ok = nullptr);
    QByteArray read(int maxSize, int * timeout, const QString & address = QString(), bool * ok = nullptr);

    int read(char * data, int maxSize, int timeout = -1, const QString & address = QString());
    int read(char * data, int maxSize, int * timeout, const QString & address = QString());

    int write(const QByteArray & datagram, const QString & address);
    int write(const QByteArray & datagram, int timeout, const QString & address);
    int write(const QByteArray & datagram, int * timeout, const QString & address);

    int write(const char * data, int size, const QString & address);
    int write(const char * data, int size, int timeout, const QString & address);
    int write(const char * data, int size, int * timeout, const QString & address);

private:
    Q_DISABLE_COPY(Interface)
    Q_DECLARE_PRIVATE(Interface)
    QScopedPointer<InterfacePrivate> d_ptr;
};

}
