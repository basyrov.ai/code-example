#include "tcp-impl.h"

#include <QDebug>
#include <QDomElement>
#include <QElapsedTimer>
#include <netinet/tcp.h>

#include <core/sys.h>

namespace pairing {

TcpImpl::TcpImpl()
{}

bool TcpImpl::open(const QString & configString)
{
    m_config.setContent(configString);

    bool result = true;
    QDomNodeList addrNodes = m_config.addresses();

    for (int i = 0; i < addrNodes.size(); ++i)
    {
        const auto addrKey = addrNodes.at(i).toElement().attribute("key");

        auto socket = TcpImpl::socket(addrKey);
        if (socket < 0) {
            result = false;
            continue;
        }

        auto val = 1;
        if ( !pairing::sys::setsockopt(socket, IPPROTO_TCP, TCP_NODELAY, reinterpret_cast<void*>(&val), sizeof(val)) )
        {
            result = false;
            continue;
        }

        QString addrStr = m_config.property("address", addrKey).attribute("value");
        bool multicast  = m_config.property("multicast", addrKey).attribute("value").toInt();

        if (multicast)
        {
            struct ip_mreq mreq;
            mreq.imr_multiaddr.s_addr = inet_addr(addrStr.toLatin1().data());
            mreq.imr_interface.s_addr = htonl(INADDR_ANY);

            if ( !pairing::sys::setsockopt(socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, reinterpret_cast<void*>(&mreq), sizeof(mreq)) )
            {
                result = false;
                continue;
            }

        }


        const auto server = m_config.property("server").attribute("value").toInt();

        if (server)
        {
            auto addr = socketAddress(addrKey);
            if (!pairing::sys::bind(socket, reinterpret_cast<struct sockaddr *>(&addr), sizeof(addr)))
            {
                pairing::sys::close(socket);
                result = false;
                continue;
            }
        }

        m_sockets[addrKey] = socket;
    }

    return result;
}

bool TcpImpl::close()
{
    bool result = true;

    for(const auto socket : m_sockets) {
        result &= pairing::sys::close(socket);
    }
    for(const auto socket : m_incomingConnections) {
        result &= pairing::sys::close(socket);
    }

    m_sockets.clear();
    m_incomingConnections.clear();

    return result;
}

int TcpImpl::read(char * data, int maxSize, int timeout, const QString & address)
{
    int socket = -1;

    const auto server = m_config.property("server").attribute("value").toInt();
    if (server)
    {
        socket = TcpImpl::incomingConnection(address, &timeout);
        if (socket < 0) { return -1; }
    }
    else
    {
        socket = TcpImpl::connection(address);
        if (socket < 0) { return -1; }
    }

    return pairing::sys::recv(socket, data, maxSize, 0, timeout);
}

QByteArray TcpImpl::read(int maxSize, int timeout, const QString & address, bool * ok)
{
    int socket = -1;

    const auto server = m_config.property("server").attribute("value").toInt();
    if (server)
    {
        socket = TcpImpl::incomingConnection(address, &timeout);
        if (socket < 0) {
            if (ok) { *ok = false; }
            return QByteArray();
        }
    }
    else
    {
        socket = TcpImpl::connection(address);
        if (socket < 0) {
            if (ok) { *ok = false; }
            return QByteArray();
        }
    }

    return pairing::sys::recv(socket, maxSize, 0, timeout, ok);
}

int TcpImpl::write(const char * data, int size, int timeout, const QString & address)
{
    int socket = -1;

    const auto server = m_config.property("server").attribute("value").toInt();
    if (server)
    {
        socket = TcpImpl::incomingConnection(address, &timeout);
        if (socket < 0) { return -1; }
    }
    else
    {
        socket = TcpImpl::connection(address);
        if (socket < 0) { return -1; }
    }

    return int(pairing::sys::send(socket, data, size_t(size), timeout));
}

void TcpImpl::close(const QString & address)
{
    auto socketIt = m_sockets.find(address);
    if (socketIt != m_sockets.end())
    {
        pairing::sys::close(*socketIt);
        m_sockets.erase(socketIt);
    }
}

int TcpImpl::socket(const QString & address)
{
    auto socketIt = m_sockets.find(address);
    if (socketIt == m_sockets.end())
    {
        int socket = pairing::sys::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

        if (socket < 0)                                                     { return -1; }
        if (!pairing::sys::setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, 1)) { return -1; }
        if (!pairing::sys::setsockopt(socket, SOL_SOCKET, SO_REUSEPORT, 1)) { return -1; }

        return *(m_sockets.insert(address, socket));
    }
    return *socketIt;
}

sockaddr_in TcpImpl::socketAddress(const QString & address)
{
    auto addrIt = m_addresses.find(address);
    if (addrIt == m_addresses.end())
    {
        quint16 port    = m_config.property("port", address).attribute("value").toUShort();
        QString addrStr = m_config.property("address", address).attribute("value");

        struct sockaddr_in addr;
        ::bzero(&addr, sizeof(addr));

        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);

        if (!pairing::sys::inet_aton(addrStr.toLatin1().data(), &addr.sin_addr)) {
            return sockaddr_in();
        }

        return *(m_addresses.insert(address, addr));
    }
    return *addrIt;
}

bool TcpImpl::isConnected(int socket)
{
    if (pairing::sys::getsockopt(socket, SOL_SOCKET, SO_ERROR) != 0) {
        return false;
    }

    char buf = 0;
    if (pairing::sys::rselect(socket, 0) &&
        pairing::sys::recv(socket, &buf, 1, MSG_DONTWAIT | MSG_PEEK) <= 0)
    {
        return false;
    }

    return true;
}

int TcpImpl::incomingConnection(const QString & address, int * timeout)
{
    Q_ASSERT(timeout);

    auto connectionIt = m_incomingConnections.find(address);
    if (connectionIt == m_incomingConnections.end())
    {
        const auto socket = TcpImpl::socket(address);
        if (socket < 0) {
            return -1;
        }

        const auto climit = m_config.property("climit").attribute("value").toInt();
        const auto newConnection = waitForConnection(socket, climit, timeout);

        if (newConnection < 0) { return -1; }

        return *(m_incomingConnections.insert(address, newConnection));
    }

    if (!isConnected(*connectionIt))
    {
        pairing::sys::close(*connectionIt);
        m_incomingConnections.erase(connectionIt);
        return incomingConnection(address, timeout);
    }

    return *connectionIt;
}

int TcpImpl::waitForConnection(int socket, int backlog, int * timeout) const
{
    Q_ASSERT(timeout);

    if (pairing::sys::listen(socket, backlog))
    {
        QElapsedTimer timer;
        timer.start();

        if (pairing::sys::rselect(socket, *timeout))
        {
            *timeout -= timer.elapsed() * 1000;
            return pairing::sys::accept(socket, nullptr, nullptr);
        }

        *timeout = 0;
    }

    return -1;
}

int TcpImpl::connection(const QString & address)
{
    auto socket = TcpImpl::socket(address);

    if (!isConnected(socket))
    {
        socket = reconnect(address);
        if (socket == -1) {
            return -1;
        }
    }

    return socket;
}

int TcpImpl::reconnect(const QString & address)
{
    close(address);

    auto sock = TcpImpl::socket(address);
    auto addr = TcpImpl::socketAddress(address);

    if ( pairing::sys::connect(sock, reinterpret_cast<struct sockaddr *>(&addr), sizeof(addr)) ) {
        return sock;
    }

    return -1;
}

}
