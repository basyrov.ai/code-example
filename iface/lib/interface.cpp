#include "interface.h"
#include "interface-private.h"

#include <QDebug>
#include <QDir>
#include <QPluginLoader>
#include <QElapsedTimer>
#include <QDomDocument>

#include "config.h"

// TODO: peek()
// TODO: debug

namespace pairing {

//! Унифицированная библиотека сопряжения
/*!
 * \class Interface
 *
 * Обеспечивает прозрачное взаимодействие с устройствами, подключенными
 * через различные физические интерфейсы. Конфигурация осуществляется через
 * настроечные XML файлы, где указываются параметры интерфейса и список
 * адресов. Редактирование файла может осуществляться через графическую
 * утилиту interface-configurator. Библиоткека может расширяться
 * путем добавления новых плагинов.
 * Поддерживаемые интерфейсы: ethernet, manchester, serial, potential
 */

//! Деструктор - закрывает интерфейс
Interface::~Interface()
{
    Q_D(Interface);
    if(d->opened) {
        close();
    }
}

//! Конструктор по умолчанию - интерфейс закрыт
Interface::Interface() :
    d_ptr(new InterfacePrivate)
{}

//! Конструктор - открывает интерфейс
/*!
 * \param configFilePath Путь к конфигурационному XML файлу
 */
Interface::Interface(const QString & configFilePath) :
    d_ptr(new InterfacePrivate)
{
    open(configFilePath);
}

//! \name Базовый API
//@{
//! Открытие интерфейса
/*!
 * \param configFilePath Путь к конфигурационному XML файлу
 * \return Признак успешного завершения
 */
bool Interface::open(const QString & configFilePath)
{
    Q_D(Interface);
    if(d->opened) {
        qCritical() << Q_FUNC_INFO << "Interface is already opened";
        return false;
    }

    Config config(configFilePath);

    QString interfaceName = config.interfaceName();
    if(!d->loadImplementation(interfaceName)) {
        qCritical() << Q_FUNC_INFO << interfaceName << "plugin loading failed";
        return false;
    }

    d->serial = config.property("serial").attribute("value", "0").toInt();
    d->serialBufferSize = config.property("sbuffer").attribute("value", QString::number(SERIAL_BUFFER_SIZE)).toLongLong();

    d->debug = config.property("debug").attribute("value", "0").toInt();
    d->debugFormat = ( !config.property("dword").isNull() || !config.property("dbase").isNull() );
    d->debugFormatWord = config.property("dword").attribute("value", "4").toInt();
    d->debugFormatBase = config.property("dbase").attribute("value", "16").toInt();

    qInfo().noquote() << config;

    d->opened = d->interfaceImpl->open(config.doc().toString());
    return d->opened;
}

//! Закрытие интерфейса
/*!
 * \return Признак успешного завершения
 * \attention Вызывается автоматически при уничтожении объекта
 */
bool Interface::close()
{
    Q_D(Interface);
    if(!d->opened) {
        qCritical() << Q_FUNC_INFO << "Interface isn't opened";
        return false;
    }

    if(d->interfaceImpl->close())
    {
        d->interfaceImpl.reset();
        d->opened = false;
        return true;
    }

    return false;
}

//! Отключение режима эмуляции последовательного интерфейса
/*!
 * По умолчанию отключен
 */
Interface & Interface::noserial()
{
    Q_D(Interface);
    d->serial = false;
    return *this;
}

//! Включение режима эмуляции последовательного интерфейса
/*!
 * По умолчанию отключен
 * \attention Размер буффера устанавливается препроцессором
 * через SERIAL_BUFFER_SIZE
 */
Interface & Interface::serial()
{
    Q_D(Interface);
    d->serial = true;
    d->serialBufferSize = SERIAL_BUFFER_SIZE;
    return *this;
}

//! Включение режима эмуляции последовательного интерфейса
/*!
 * По умолчанию отключен
 * \param bufferSize Размер буффера
 */
Interface & Interface::serial(int bufferSize)
{
    Q_D(Interface);
    d->serial = true;
    d->serialBufferSize = bufferSize;
    return *this;
}

//! Отключение режима отладки
/*!
 * По умолчанию отключен
 */
Interface & Interface::nodebug()
{
    Q_D(Interface);
    d->debug = false;
    return *this;
}

//! Режим отладки
/*!
 * Весь ввод/вывод транслируется в qDebug() в неформатированном виде
 */
Interface & Interface::debug()
{
    Q_D(Interface);
    d->debug = true;
    d->debugFormat = false;
    return *this;
}

//! Режим отладки
/*!
 * Весь ввод/вывод транслируется в qDebug() в форматированном виде
 * \param word Длина слова данных в байтах
 * \param base Основание выводимых данных
 */
Interface & Interface::debug(int word, int base)
{
    Q_D(Interface);
    d->debug = true;
    d->debugFormat = true;
    d->debugFormatWord = word;
    d->debugFormatBase = base;
    return *this;
}

//! Получение состояния интерфейса
/*!
 * \return Признак открытого интерфейса
 */
bool Interface::isOpened() const
{
    Q_D(const Interface);
    return d->opened;
}

//! Получение состояния режима эмуляции последовательного интерфейса
/*!
 * \return Признак включенной эмуляции последовательного интерфейса
 */
bool Interface::isSerial() const
{
    Q_D(const Interface);
    return d->serial;
}

//! Получение состояния режима отладки
/*!
 * \return Признак включенного режима отладки
 */
bool Interface::isDebug() const
{
    Q_D(const Interface);
    return d->debug;
}

//@}

//! \name API ввода-вывода
//@{

//! Чтение всех доступных данных
/*!
 * \param timeout Таймаут соединения, микросекунды
 * \param address Ключ адреса
 * \param ok указатель на переменную для установки признака успешного завершения
 * \return Считанная датаграмма
 */
QByteArray Interface::readAll(int timeout, const QString & address, bool * ok)
{
    return read(0, timeout, address, ok);
}

//! Чтение всех доступных данных
/*!
 * \param timeout Указатель на переменную таймаута соединения в микросекундах
 * \param address Ключ адреса
 * \param ok указатель на переменную для установки признака успешного завершения
 * \return Считанная датаграмма
 * \attention Значение timeout уменьшается на затраченное время чтения
 */
QByteArray Interface::readAll(int * timeout, const QString & address, bool * ok)
{
    return read(0, timeout, address, ok);
}

//! Чтение данных
/*!
 * \param maxSize Максимальное количество считываемых байт, 0 - считать все
 * \param timeout Таймаут соединения, микросекунды
 * \param address Ключ адреса
 * \param ok указатель на переменную для установки признака успешного завершения
 * \return Считанная датаграмма
 */
QByteArray Interface::read(int maxSize, int timeout, const QString & address, bool * ok)
{
    Q_D(Interface);

    if(d->opened)
    {
        if (d->debug) { qDebug() << "[interface] Read" << maxSize << "bytes from" << address; }
        if (d->serial)
        {
            const auto freeSpace = d->serialBufferSize - d->serialBuffer.size();
            if (freeSpace >= 0) {
                d->serialBuffer.enqueue( d->interfaceImpl->read(freeSpace, timeout, address.isEmpty() ? "default" : address, ok) );
            }
            return d->serialBuffer.dequeue( (maxSize ? maxSize : d->serialBuffer.size()) );
        }
        else
        {
            return d->interfaceImpl->read(maxSize, timeout, address.isEmpty() ? "default" : address, ok);
        }
    }
    else {
        if (ok) { *ok = false; }
        qCritical() << Q_FUNC_INFO << "Trying to read from" << address << "in closed state";
    }

    return QByteArray();
}

//! Чтение данных
/*!
 * \param maxSize Максимальное количество считываемых байт, 0 - считать все
 * \param timeout Указатель на переменную таймаута соединения в микросекундах
 * \param address Ключ адреса
 * \param ok указатель на переменную для установки признака успешного завершения
 * \return Считанная датаграмма
 * \attention Значение timeout уменьшается на затраченное время чтения
 */
QByteArray Interface::read(int maxSize, int * timeout, const QString & address, bool * ok)
{
    Q_ASSERT(timeout);

    QElapsedTimer timer;
    timer.start();
    QByteArray datagram = read(maxSize, *timeout, address, ok);

    auto elapsed = timer.elapsed() * 1000;
    *timeout -= ((elapsed > *timeout) ? *timeout : elapsed);

    return datagram;
}

//! Чтение данных
/*!
 * \param data Указатель на буфер
 * \param maxSize Максимальное количество считываемых байт
 * \param timeout Таймаут соединения, микросекунды
 * \param address Ключ адреса
 * \return Количество считанных байт или -1 в случае ошибки
 */
int Interface::read(char *data, int maxSize, int timeout, const QString &address)
{
    Q_D(Interface);
    if(d->opened)
    {
        if (d->debug) { qDebug() << "[interface] Read" << maxSize << "bytes from" << address; }
        if (d->serial)
        {
            const auto freeSpace = d->serialBufferSize - d->serialBuffer.size();
            if (freeSpace >= 0) {
                d->serialBuffer.enqueue( d->interfaceImpl->read(freeSpace, timeout, address.isEmpty() ? "default" : address, nullptr) );
            }
            return d->serialBuffer.dequeue(data, maxSize);
        }
        else
        {
            return d->interfaceImpl->read(data, maxSize, timeout, address.isEmpty() ? "default" : address);
        }
    }
    else
    {
        qCritical() << Q_FUNC_INFO << "Trying to read from" << address << "in closed state";
    }
    return -1;
}

//! Чтение данных
/*!
 * \param data Указатель на буфер
 * \param maxSize Максимальное количество считываемых байт
 * \param timeout Указатель на переменную таймаута соединения в микросекундах
 * \param address Ключ адреса
 * \return Количество считанных байт или -1 в случае ошибки
 * \attention Значение timeout уменьшается на затраченное время чтения
 */
int Interface::read(char *data, int maxSize, int *timeout, const QString &address)
{
    Q_ASSERT(timeout);

    QElapsedTimer timer;
    timer.start();
    int result = read(data, maxSize, *timeout, address);

    auto elapsed = timer.elapsed() * 1000;
    *timeout -= ((elapsed > *timeout) ? *timeout : elapsed);

    return result;
}

//! Запись данных
/*!
 * \param datagram Записываемая датаграмма
 * \param address Ключ адреса
 * \return Количество записанных байт или -1 в случае ошибки
 */
int Interface::write(const QByteArray & datagram, const QString & address)
{
    return write(datagram, -1, address);
}

//! Запись данных
/*!
 * \param datagram Записываемая датаграмма
 * \param timeout Таймаут соединения, микросекунды
 * \param address Ключ адреса
 * \return Количество записанных байт или -1 в случае ошибки
 */
int Interface::write(const QByteArray &datagram, int timeout, const QString &address)
{
    return write(datagram.data(), datagram.size(), timeout, address);
}

//! Запись данных
/*!
 * \param datagram Записываемая датаграмма
 * \param timeout Указатель на переменную таймаута соединения в микросекундах
 * \param address Ключ адреса
 * \return Количество записанных байт или -1 в случае ошибки
 */
int Interface::write(const QByteArray &datagram, int *timeout, const QString &address)
{
    return write(datagram.data(), datagram.size(), timeout, address);
}

//! Запись данных
/*!
 * \param data Указатель на данные
 * \param size Размер записываемых данных в байтах
 * \param address Ключ адреса
 * \return Количество записанных байт или -1 в случае ошибки
 */
int Interface::write(const char * data, int size, const QString & address)
{
    return write(data, size, -1, address);
}

//! Запись данных
/*!
 * \param data Указатель на данные
 * \param size Размер записываемых данных в байтах
 * \param timeout Таймаут соединения, микросекунды
 * \param address Ключ адреса
 * \return Количество записанных байт или -1 в случае ошибки
 */
int Interface::write(const char *data, int size, int timeout, const QString &address)
{
    Q_D(Interface);

    if (size <= 0) { return 0; }
    if (d->debug) { qDebug() << "[interface] Write" << size << "bytes to" << address; }

    if(d->opened) {
        return d->interfaceImpl->write(data, size, timeout, address.isEmpty() ? "default" : address);
    } else {
        qCritical() << Q_FUNC_INFO << "Trying to write from" << address << "in closed state";
    }

    return -1;
}

//! Запись данных
/*!
 * \param data Указатель на данные
 * \param size Размер записываемых данных в байтах
 * \param timeout Указатель на переменную таймаута соединения в микросекундах
 * \param address Ключ адреса
 * \return Количество записанных байт или -1 в случае ошибки
 */
int Interface::write(const char *data, int size, int *timeout, const QString &address)
{
    Q_ASSERT(timeout);

    QElapsedTimer timer;
    timer.start();
    int result = write(data, size, *timeout, address);

    auto elapsed = timer.elapsed() * 1000;
    *timeout -= ((elapsed > *timeout) ? *timeout : elapsed);

    return result;
}

//@}

InterfacePrivate::InterfacePrivate()
{}

AbstractInterface * InterfacePrivate::loadImplementation(const QString &interfaceName)
{
    QPluginLoader pluginLoader;
    QDir pluginsDir(PLUGIN_DIR);
    pluginLoader.setFileName(pluginsDir.filePath(QString("lib%1.so").arg(interfaceName)));

    if (pluginLoader.load())
    {
        if (AbstractInterface *interface = qobject_cast<AbstractInterface *>(pluginLoader.instance()))
        {
            interfaceImpl.reset(interface->createAnother());
            return interfaceImpl.data();
        }
        else
        {
            qCritical() << Q_FUNC_INFO << "Plugin loading failed:" << pluginLoader.errorString();
        }
    }
    else
    {
        qCritical() << Q_FUNC_INFO << "Plugin loading failed:" << pluginLoader.errorString();
    }

    return nullptr;
}

}
