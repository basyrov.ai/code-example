#pragma once

#include <QScopedPointer>
#include <QBuffer>

#include <plugins/abstract-interface.h>

#include "buffer.h"

#ifndef SERIAL_BUFFER_SIZE
#define SERIAL_BUFFER_SIZE 65536
#endif

class QString;
class QPluginLoader;

namespace pairing {

class InterfacePrivate {
public:
    QScopedPointer<AbstractInterface> interfaceImpl;
    bool opened = false;

    bool serial = false;
    int serialBufferSize = SERIAL_BUFFER_SIZE;
    Buffer serialBuffer;

    bool debug = false;
    bool debugFormat = false;
    int debugFormatWord = 4;
    int debugFormatBase = 16;

    bool hasErrors = false;
    QString errorString;

public:
    InterfacePrivate();
    AbstractInterface * loadImplementation(const QString & interfaceName);
};

}
