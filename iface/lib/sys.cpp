#include "sys.h"

#include <QDebug>

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

namespace pairing { namespace sys {

int socket(int domain, int type, int protocol)
{
    const auto socket = ::socket(domain, type, protocol);
    if (socket < 0)
    {
        qCritical() << Q_FUNC_INFO << "Socket creation failed:" << ::strerror(errno);
    }
    return socket;
}

bool setsockopt(int descriptor, int level, int optname, const void * optval, socklen_t optlen)
{
    if (::setsockopt(descriptor, level, optname, optval, optlen) == -1)
    {
        qCritical() << Q_FUNC_INFO << "Socket" << optname << "opt setting failed:" << ::strerror(errno);
        return false;
    }
    return true;
}

bool setsockopt(int descriptor, int level, int optname, int optval)
{
    return pairing::sys::setsockopt(descriptor, level, optname, &optval, sizeof(optval));
}

int getsockopt(int descriptor, int level, int optname)
{
    int optval = 0;
    socklen_t optlen = 0;
    const auto result = ::getsockopt(descriptor, level, optname, &optval, &optlen);
    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Socket" << optname << "opt getting failed:" << ::strerror(errno);
        return -1;
    }
    return optval;
}

bool inet_aton(const char *addrStr, struct in_addr * addr)
{
    if (::inet_aton(addrStr, addr) == 0)
    {
        qCritical() << Q_FUNC_INFO << QString::fromLatin1(addrStr) << "converting failed:" << ::strerror(errno);
        return false;
    }
    return true;
}

bool bind(int descriptor, const struct sockaddr *addr, socklen_t len)
{
    if (::bind(descriptor, addr, len) == -1)
    {
        qCritical() << Q_FUNC_INFO << "Socket binding failed:" << ::strerror(errno);
        return false;
    }
    return true;
}

int open(const QString &devpath, int flags)
{
    const auto descriptor = ::open(devpath.toLatin1().data(), flags);
    if(descriptor == -1) {
        qCritical() << Q_FUNC_INFO << devpath << "opening failed :" << ::strerror(errno);;
    }
    return descriptor;
}

bool close(int descriptor)
{
    const auto result = ::close(descriptor);
    if (result == -1) {
        qCritical() << Q_FUNC_INFO << descriptor << "closing failed :" << ::strerror(errno);
        return false;
    }
    return true;
}

struct timeval timeval(int usec)
{
    struct timeval timeoutval;
    timeoutval.tv_sec = usec/1000000;
    timeoutval.tv_usec = usec - timeoutval.tv_sec*1000000;
    return timeoutval;
}

bool rselect(int descriptor, int usec, bool * ok)
{
    if (ok) { *ok = true; }

    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(descriptor, &rfds);

    auto tv = timeval(usec);
    const auto result = ::select(descriptor + 1, &rfds, nullptr, nullptr, usec >= 0 ? &tv : nullptr);

    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Read select failed :" << ::strerror(errno);
        if (ok) { *ok = false; }
        return false;
    }
    else if (result == 0)
    {
        return false;
    }
    return true;
}

bool wselect(int descriptor, int usec, bool * ok)
{
    if (ok) { *ok = true; }

    fd_set wfds;
    FD_ZERO(&wfds);
    FD_SET(descriptor, &wfds);

    struct timeval timeoutval = timeval(usec);
    const auto result = ::select(descriptor + 1, nullptr, &wfds, nullptr, usec >= 0 ? &timeoutval : nullptr);

    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Write select failed :" << ::strerror(errno);
        if (ok) { *ok = false; }
        return false;
    }
    else if (result == 0)
    {
        return false;
    }

    return true;
}

int pendingSize(int descriptor)
{
    int pendingSize = 0;
    if (::ioctl(descriptor, FIONREAD, &pendingSize) == -1)
    {
        qCritical() << Q_FUNC_INFO << "IO control failed:" << ::strerror(errno);
        return -1;
    }
    return pendingSize;
}

ssize_t read(int descriptor, char * data, size_t size)
{
    // TODO: check SSIZE_MAX
    const auto result = ::read(descriptor, data, size);
    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Read failed :" << ::strerror(errno);
    }
    return result;
}

int read(int descriptor, char * data, int maxSize, int timeout)
{
    bool ok = true;
    if (pairing::sys::rselect(descriptor, timeout, &ok)) {
        return pairing::sys::read(descriptor, data, size_t(maxSize));
    }
    return (ok ? 0 : -1);
}

QByteArray read(int descriptor, int maxSize, int timeout, bool * ok)
{
    if (pairing::sys::rselect(descriptor, timeout, ok))
    {
        if (ok) { *ok = true; }

        const auto pendingSize = pairing::sys::pendingSize(descriptor);
        if (pendingSize > 0)
        {
            QByteArray datagram(maxSize == 0 ? pendingSize
                                             : (maxSize > pendingSize ? pendingSize : int(maxSize)), 0);

            const auto result = pairing::sys::read(descriptor, datagram.data(), size_t(datagram.size()));

            if (result > 0)
            {
                return datagram;
            }
            else if (result < 0)
            {
                if (ok) { *ok = false; }
            }
        }
    }
    return QByteArray();
}

ssize_t recv(int socket, char * data, size_t size, int flags)
{
    // TODO: check SSIZE_MAX
    const auto result = ::recv(socket, data, size, flags);
    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Read failed :" << ::strerror(errno);
    }
    return result;
}

int recv(int socket, char * data, int maxSize, int flags, int timeout)
{
    bool ok = true;
    if (pairing::sys::rselect(socket, timeout, &ok)) {
        return pairing::sys::recv(socket, data, size_t(maxSize), flags);
    }
    return (ok ? 0 : -1);
}

QByteArray recv(int socket, int maxSize, int flags, int timeout, bool * ok)
{
    if (pairing::sys::rselect(socket, timeout, ok))
    {
        if (ok) { *ok = true; }

        auto pendingSize = pairing::sys::pendingSize(socket);
        if (pendingSize > 0)
        {
            QByteArray datagram(maxSize == 0 ? pendingSize
                                             : (maxSize > pendingSize ? pendingSize : int(maxSize)), 0);

            const auto result = pairing::sys::recv(socket, datagram.data(), size_t(datagram.size()), flags);

            if (result > 0)
            {
                return datagram;
            }
            else if (result < 0)
            {
                if (ok) { *ok = false; }
            }
        }
    }
    return QByteArray();
}

ssize_t write(int descriptor, const char * data, size_t size)
{
    const auto result = ::write(descriptor, data, size);
    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Write failed :" << ::strerror(errno);
    }
    return result;
}

int write(int descriptor, const char * data, int size, int timeout)
{
    bool ok = true;
    if (pairing::sys::wselect(descriptor, timeout, &ok)) {
        return pairing::sys::write(descriptor, data, size_t(size));
    }
    return (ok ? 0 : -1);
}

ssize_t send(int descriptor, const char * data, size_t size)
{
    const auto result = ::send(descriptor, data, size, 0);
    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Send failed :" << ::strerror(errno);
    }
    return result;
}

ssize_t send(int descriptor, const char * data, size_t size, int timeout)
{
    bool ok = true;
    if (pairing::sys::wselect(descriptor, timeout, &ok))
    {
        return pairing::sys::send(descriptor, data, size);
    }
    return (ok ? 0 : -1);
}

ssize_t sendto(int descriptor, const char * data, size_t size, sockaddr * addr, socklen_t addrSize)
{
    const auto result = ::sendto(descriptor, data, size, 0, addr, addrSize);
    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Sendto failed :" << ::strerror(errno);
    }
    return result;
}

ssize_t sendto(int descriptor, const char * data, size_t size, sockaddr_in * addr, int timeout)
{
    bool ok = true;
    if (pairing::sys::wselect(descriptor, timeout, &ok))
    {
        return pairing::sys::sendto(descriptor, data, size, reinterpret_cast<struct sockaddr *>(addr), (addr ? sizeof(sockaddr_in) : 0));
    }
    return (ok ? 0 : -1);
}

bool connect(int descriptor, const struct sockaddr * addr, socklen_t addrlen)
{
    const auto result = ::connect(descriptor, addr, addrlen);
    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Connect failed :" << ::strerror(errno);
        return false;
    }
    return true;
}

bool listen(int descriptor, int backlog)
{
    const auto result = ::listen(descriptor, backlog);
    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Listen failed :" << ::strerror(errno);
        return false;
    }
    return true;
}

int accept(int descriptor, struct sockaddr * addr, socklen_t * addrlen)
{
    const auto result = ::accept(descriptor, addr, addrlen);
    if (result == -1)
    {
        qCritical() << Q_FUNC_INFO << "Accept failed :" << ::strerror(errno);
    }
    return result;
}

}}
