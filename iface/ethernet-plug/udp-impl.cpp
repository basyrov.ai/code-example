#include "udp-impl.h"

#include <QDebug>
#include <QDomElement>

#include <core/sys.h>

namespace pairing {

UdpImpl::UdpImpl()
{}

bool UdpImpl::open(const QString & configString)
{
    m_config.setContent(configString);

    bool ok = true;
    QDomNodeList addrNodes = m_config.addresses();
    for (int i = 0; i < addrNodes.size(); ++i)
    {
        int socket = pairing::sys::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

        if (socket < 0)                                                      { ok = false; continue; }
        if (!pairing::sys::setsockopt(socket, SOL_SOCKET, SO_REUSEPORT, 1))  { ok = false; continue; }
        if (!pairing::sys::setsockopt(socket, SOL_SOCKET, SO_BROADCAST, 1))  { ok = false; continue; }


        QDomElement addrNode = addrNodes.at(i).toElement();
        QString addrKey      = addrNode.attribute("key");
        int port             = m_config.property("port", addrKey).attribute("value").toInt();
        QString addrStr      = m_config.property("address", addrKey).attribute("value");
        bool multicast       = m_config.property("multicast", addrKey).attribute("value").toInt();


        if (multicast)
        {
            struct ip_mreq mreq;
            mreq.imr_multiaddr.s_addr = inet_addr(addrStr.toLatin1().data());
            mreq.imr_interface.s_addr = htonl(INADDR_ANY);

            if ( !pairing::sys::setsockopt(socket, IPPROTO_IP, IP_ADD_MEMBERSHIP, reinterpret_cast<void*>(&mreq), sizeof(mreq)) )
            {
                ok = false;
                continue;
            }

        }


        struct sockaddr_in & addr = m_addresses[addrKey];
        ::bzero(&addr, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);

        if (!pairing::sys::inet_aton(addrStr.toLatin1().data(), &addr.sin_addr))
        {
            ok = false;
            continue;
        }

        if (addrNode.attribute("mode").contains("r"))
        {
            if (!pairing::sys::bind(socket, reinterpret_cast<struct sockaddr *>(&addr), sizeof(addr)))
            {
                pairing::sys::close(socket);
                ok = false;
                continue;
            }
        }

        m_sockets[addrKey] = socket;
    }

    return ok;
}

bool UdpImpl::close()
{
    bool result = true;
    for(const auto socket : m_sockets) {
        result &= pairing::sys::close(socket);
    }
    m_sockets.clear();
    return result;
}

int UdpImpl::read(char * data, int maxSize, int timeout, const QString & address)
{
    const auto socket = UdpImpl::socket(address);
    if (socket < 0) { return -1; }

    return pairing::sys::read(socket, data, maxSize, timeout);
}

QByteArray UdpImpl::read(int maxSize, int timeout, const QString & address, bool * ok)
{
    const auto socket = UdpImpl::socket(address);

    if (socket < 0) {
        if (ok) { *ok = false; }
        return QByteArray();
    }

    return pairing::sys::read(socket, maxSize, timeout, ok);
}

int UdpImpl::write(const char * data, int size, int timeout, const QString & address)
{
    const auto socket = UdpImpl::socket(address);
    if (socket < 0) { return -1; }

    sockaddr_in addr = UdpImpl::addr(address);
    return int(pairing::sys::sendto(socket, data, size_t(size), &addr, timeout));
}

int UdpImpl::socket(const QString & address) const
{
    auto socket = m_sockets.find(address);
    if (socket == m_sockets.end()) {
        qCritical() << Q_FUNC_INFO << "No socket opened on address" << address;
        return -1;
    }
    return *socket;
}

sockaddr_in UdpImpl::addr(const QString & address) const
{
    auto addr = m_addresses.find(address);
    if (addr == m_addresses.end())
    {
        qCritical() << Q_FUNC_INFO << "No address found" << address;
        return sockaddr_in {};
    }
    return *addr;
}

}
