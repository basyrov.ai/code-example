#include "ethernet-interface.h"

#include <QDebug>
#include <QDomElement>

#include <core/config.h>
#include <core/sys.h>
#include <core/utils/file-string.h>

#include "udp-impl.h"
#include "tcp-impl.h"

namespace pairing {

EthernetInterface::EthernetInterface(QObject * parent) :
    QObject(parent)
{}

QString EthernetInterface::config() const
{
    return utils::FileString(":ethernet/config.xml");
}

bool EthernetInterface::open(const QString & configString)
{
    Config config;
    config.setContent(configString);

    const auto & protocol = config.property("protocol").attribute("value");

    if (protocol == "udp")
    {
        m_impl = new UdpImpl();
        return m_impl->open(configString);
    }
    else if (protocol == "tcp")
    {
        m_impl = new TcpImpl();
        return m_impl->open(configString);
    }
    else
    {
        qCritical() << Q_FUNC_INFO << "Unknown protocol" << protocol;
    }

    return false;
}

bool EthernetInterface::close()
{
    return m_impl->close();
}

int EthernetInterface::read(char * data, int maxSize, int timeout, const QString & address)
{
    return m_impl->read(data, maxSize, timeout, address);
}

QByteArray EthernetInterface::read(int maxSize, int timeout, const QString & address, bool * ok)
{
    return m_impl->read(maxSize, timeout, address, ok);
}

int EthernetInterface::write(const char * data, int size, int timeout, const QString & address)
{
    return m_impl->write(data, size, timeout, address);
}

AbstractInterface * EthernetInterface::createAnother() const
{
    return new EthernetInterface();
}

}
