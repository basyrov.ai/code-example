#pragma once

#include <QMap>
#include <arpa/inet.h>

#include <plugins/abstract-interface.h>
#include <core/config.h>

namespace pairing {

class TcpImpl : public AbstractInterface {
public:
    TcpImpl();

    virtual bool open(const QString & configString) override;
    virtual bool close() override;

    virtual int read(char * data, int maxSize, int timeout, const QString & address) override;
    virtual QByteArray read(int maxSize, int timeout, const QString & address, bool * ok) override;

    virtual int write(const char *data, int size, int timeout, const QString &address) override;

private:
    virtual QString config() const override { return QString(); }
    virtual AbstractInterface * createAnother() const override { return nullptr; }

private:
    void close(const QString & address);
    int socket(const QString & address);
    sockaddr_in socketAddress(const QString & address);

    bool isConnected(int socket);

    int incomingConnection(const QString & address, int * timeout);
    int waitForConnection(int socket, int backlog, int * timeout) const;

    int connection(const QString & address);
    int reconnect(const QString & address);

private:
    Config m_config;
    QMap<QString,sockaddr_in> m_addresses;
    QMap<QString,int> m_sockets;
    QMap<QString,int> m_incomingConnections;
};

}
