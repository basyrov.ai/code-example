#pragma once

#include <QMap>
#include <arpa/inet.h>

#include <plugins/abstract-interface.h>
#include <core/config.h>

namespace pairing {

class UdpImpl : public AbstractInterface {
public:
    UdpImpl();

    virtual bool open(const QString &configString) override;
    virtual bool close() override;

    virtual int read(char *data, int maxSize, int timeout, const QString &address) override;
    virtual QByteArray read(int maxSize, int timeout, const QString &address, bool * ok) override;

    virtual int write(const char *data, int size, int timeout, const QString &address) override;

private:
    virtual QString config() const override { return QString(); }
    virtual AbstractInterface * createAnother() const override { return nullptr; }

private:
    int socket(const QString & address) const;
    sockaddr_in addr(const QString & address) const;

private:
    Config m_config;
    QMap<QString,sockaddr_in> m_addresses;
    QMap<QString,int> m_sockets;
};

}
